# REST API example #

* Mongo must be started locally

# Docker #

Build Docker image:

```bash
docker build -t rest-example .
```

Run Docker container based on image (will fail because there is no Mongo on the image):

```bash
docker run -it --rm -p 3000:3000 rest-example
```

Run docker-compose to get both containers up and running:

```bash
docker-compose up
```