'use strict';
var mongoose = require('mongoose'),
  Books = mongoose.model('Books');

exports.findAllBooks = function(req, res) {
  Books.find({}, function(err, books) {
    if (err) {
      res.send(err);
    }
    res.json(books);
  });
};

exports.findBookById = function(req, res) {
  Books.findById(req.params.bookId, function(err, book) {
    if (err) {
      res.send(err);
    } else if (book === null) {
      res.sendStatus(404);
    } else {
      res.json(book);
    }
  });
};

exports.createBook = function(req, res) {
  var new_book = new Books(req.body);
  new_book.save(function(err, book) {
    if (err) {
      res.send(err);
    }
    res.json(book);
  });
};

exports.updateBook = function(req, res) {
  Books.findOneAndUpdate({_id: req.params.bookId}, req.body, {new: true}, function(err, book) {
    if (err) {
      res.send(err);
    }
    res.json(book);
  });
};

exports.deleteBook = function(req, res) {
  Books.findById(req.params.bookId, function(err, book) {
    if (err) {
      res.send(err);
    } else if (book === null) {
      res.sendStatus(404);
    } else {
      book.remove(function(err) {
        if (err) {
          res.send(err);
        }
        res.json({ message: 'Book successfully deleted' });
      });
    }
  });
};
