'use strict';
var mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');

var BookSchema = new mongoose.Schema({
  title: String,
  author: String,
  releaseYear: Number,
  isbn: String
}, { versionKey: false });

BookSchema.plugin(autoIncrement.plugin, 'Books');
module.exports = mongoose.model('Books', BookSchema);