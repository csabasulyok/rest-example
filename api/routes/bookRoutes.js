'use strict';
var express = require('express');

var bookController = require('../controller/bookController'),
    bookValidator = require('../validator/bookValidator');

var router = express.Router();


router.route('/books')
      .get(bookController.findAllBooks)
      .post(bookController.createBook); // only checks for this single POST


router.route('/books/:bookId')
      .all(bookValidator.checkBookIdParam) // checks for all subsequent calls (get, post, delete)
      .get(bookController.findBookById)
      .put(bookController.updateBook)
      .delete(bookController.deleteBook);

module.exports = router;