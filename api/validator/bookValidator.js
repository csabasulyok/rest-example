var bookIdFormat = /^[0-9]+$/;

exports.checkBookIdParam = function(req, resp, next) {
  if (!req.params.bookId.match(bookIdFormat)) {
    resp.status(400).send({ message: "Book ID doesn't respect format " + bookIdFormat });
  } else {
    next();
  }
}
