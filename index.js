var express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    autoIncrement = require('mongoose-auto-increment'),
    waitForMongoose = require('wait-for-mongoose');

// configurable variables: server port and mongo URL
var port = process.env.PORT || 3000,
    mongoUrl = process.env.MONGO_URL || 'mongodb://localhost/Books';

var app = express();
mongoose.Promise = global.Promise;

// wait for the mongo URL to be available
// docker-compose does not ensure mongo will finish initializing before server
waitForMongoose(mongoUrl, function(err) {
    if (err) {
        console.error('Timeout connecting to MongoDB server!');
        process.exit(1);
    }

    var connection = mongoose.connect(mongoUrl, { useMongoClient: true }); 
    autoIncrement.initialize(connection);

    require('./api/model/bookModel');
    var routes = require('./api/routes/bookRoutes');
    app.use('/api', routes);
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// expose static files in "public" subfolder
app.use('/', express.static('public'));

// start listening on given port for REST calls
app.listen(port, function() {
    console.log('Books RESTful API server started on: ' + port);
});